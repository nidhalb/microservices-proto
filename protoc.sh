#!/bin/bash

SERVICE_NAME=$1

protoc --go_out=./golang --go_opt=paths=source_relative \
    --go-grpc_out=./golang --go-grpc_opt=paths=source_relative \
    ./${SERVICE_NAME}/*.proto
cd golang/${SERVICE_NAME}
go mod init gitlab.com/nidhalb/microservices-proto/golang/${SERVICE_NAME} || true
go mod tidy
cd ../../
git add . && git commit -am "proto update" || true
git push -uf origin main
